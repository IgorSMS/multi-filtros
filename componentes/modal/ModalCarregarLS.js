import {montaTolltip} from '../../js/montaComponente.js'
import {fecharModal} from './ModalSalvarLS.js'
import {novaTabela} from '../../js/tabela.js'

function carregarLista(){
    let listaNova = JSON.parse(localStorage.getItem(document.getElementById("select-listas-Carregar").value));

    novaTabela(listaNova, 'idTabela', [], 1, 10, document.getElementById("select-listas-Carregar").value);
    montaTolltip('container-tooltip', 'tootip', 'conteudo-tooltip', 'Lista carregada com sucesso!', 'green');
    fecharModal();
}

export {
    carregarLista
}