import {montaTolltip} from '../../js/montaComponente.js'
import {fecharPopUpEfeitoOpacidade, abrirPopUpEfeitoOpacidade} from '../../js/popUp.js'
import {getInfoLocalstorage, setInfoLocalstorage} from '../../js/reutilizaveis.js'
import {tabela, novaTabela, popularTabela} from '../../js/tabela.js'

function salvarLista() {
    if (document.getElementById('nome-lista').value != ""){
        if (!getInfoLocalstorage(document.getElementById('nome-lista').value)){
            setInfoLocalstorage(document.getElementById('nome-lista').value, tabela.getLista());
            montaTolltip('container-tooltip', 'tootip', 'conteudo-tooltip', 'Lista salva com sucesso!', 'green');
            fecharModal();
        }
        else{
            montaTolltip('container-tooltip', 'tootip', 'conteudo-tooltip', 'Já existe uma lista com esse nome.', 'yellow');
            document.getElementById('modal-conteudo-opcoes').classList.remove('w-285-px')
            document.getElementById('modal-conteudo-opcoes').classList.add('w-418-px')
            toogleInputNome('buttons-slvr-cnclr', 'buttons-sbrcv-cnctn');
            toogleInputNome('nome-lista', 'nome-escolhido');
            document.getElementById('nome-escolhido').innerHTML = document.getElementById('nome-lista').value;
        }
    }else{
        montaTolltip('container-tooltip', 'tootip', 'conteudo-tooltip', 'Preencha o nome pra salvar a lista!', 'red');    
    }
}

function cancelar() {
    toogleInputNome('buttons-sbrcv-cnctn', 'buttons-slvr-cnclr');
    toogleInputNome('nome-escolhido', 'nome-lista');
    resetEffectModal();
}

function sobrescrever(){
    setInfoLocalstorage(document.getElementById('nome-lista').value, tabela.getLista());
    montaTolltip('container-tooltip', 'tootip', 'conteudo-tooltip', 'Lista sobrescrita com sucesso!', 'green');
    fecharModal();
}

function concatenar(){
    toogleInputNome('buttons-sbrcv-cnctn', 'buttons-concat');
    document.getElementById('modal-conteudo-opcoes').classList.remove('hght-145-px');
    document.getElementById('modal-conteudo-opcoes').classList.add('hght-170-px');
}

function toogleInputNome(primeiroID, segundoID) {
    fecharPopUpEfeitoOpacidade(primeiroID);
    setTimeout(() => {
        abrirPopUpEfeitoOpacidade(segundoID);
    }, 250);
}

function apagarLista(){
    localStorage.removeItem(document.getElementById('nome-escolhido').innerText);
    montaTolltip('container-tooltip', 'tootip', 'conteudo-tooltip', 'Lista apagada com sucesso!', 'green');
    fecharModal();
}

function resetEffectModal() {
    document.getElementById('modal-conteudo-opcoes').classList.remove('w-418-px');
    document.getElementById('modal-conteudo-opcoes').classList.add('w-285-px');
    document.getElementById('modal-conteudo-opcoes').classList.remove('hght-170-px');
    document.getElementById('modal-conteudo-opcoes').classList.add('hght-145-px');
}

function cancelarConcat(){
    toogleInputNome('buttons-concat', 'buttons-sbrcv-cnctn');
    document.getElementById('modal-conteudo-opcoes').classList.add('hght-145-px');
    document.getElementById('modal-conteudo-opcoes').classList.remove('hght-170-px');
}

function finalizarConcat(){
    let valueRadio;
    var radios = document.getElementsByName('gender');
    for (var i = 0, length = radios.length; i < length; i++) {
        if (radios[i].checked) {
            valueRadio = radios[i].value;
            break;
        }
    }
    let listaSalva = getInfoLocalstorage(document.getElementById('nome-escolhido').innerText);
    let novaLista = [];
    let contadorConteudoNovo = 0;
    if (compareKeys(listaSalva[0], tabela.getLista()[0])){
        if (valueRadio == "repetir"){
            novaLista = listaSalva.concat(tabela.getLista());
            setInfoLocalstorage(document.getElementById('nome-escolhido').innerText, novaLista);
            novaTabela(novaLista, 'idTabela', [], 1, 10, document.getElementById('nome-escolhido').innerText);
            montaTolltip('container-tooltip', 'tootip', 'conteudo-tooltip', 'Lista concatenada com sucesso!!', 'green');
            fecharModal();
        }else{
            tabela.getLista().forEach(conteudo => {
                if (!listaSalva.find(conteudoListaSalva => compareValues(conteudo, conteudoListaSalva))){
                    novaLista.push(conteudo);
                    contadorConteudoNovo++;
                }
            });
            if (contadorConteudoNovo){
                novaLista = listaSalva.concat(novaLista);
                setInfoLocalstorage(document.getElementById('nome-escolhido').innerText, novaLista);
                novaTabela(novaLista, 'idTabela', [], 1, 10, document.getElementById('nome-escolhido').innerText);
                montaTolltip('container-tooltip', 'tootip', 'conteudo-tooltip', 'Lista concatenada com sucesso!! ' + contadorConteudoNovo + ' novas linhas', 'green');
                fecharModal();
            }else{
                montaTolltip('container-tooltip', 'tootip', 'conteudo-tooltip', 'Nenhum item adicional encontrado!!', 'yellow');
            }
        }

    }else{
        montaTolltip('container-tooltip', 'tootip', 'conteudo-tooltip', 'Error: Lista com colunas diferentes!', 'red');
    }
}

function compareKeys(objCompare, objCompare2) {
    var aKeys = Object.keys(objCompare).sort();
    var bKeys = Object.keys(objCompare2).sort();
    return JSON.stringify(aKeys) === JSON.stringify(bKeys);
}
function compareValues(objCompare, objCompare2) {
    var aValues = Object.values(objCompare).sort();
    var bValues = Object.values(objCompare2).sort();
    return JSON.stringify(aValues) === JSON.stringify(bValues);
}

function fecharModal(){
    resetEffectModal();
    fecharPopUpEfeitoOpacidade('modal-container-opcoes');
}

export {
    salvarLista, 
    cancelar, 
    sobrescrever, 
    concatenar, 
    toogleInputNome, 
    apagarLista,
    cancelarConcat,
    finalizarConcat,
    fecharModal
}