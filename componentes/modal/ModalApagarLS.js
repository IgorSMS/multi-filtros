import {montaTolltip} from '../../js/montaComponente.js'
import {fecharModal} from './ModalSalvarLS.js'

function apagarLista(){
    localStorage.removeItem(document.getElementById("select-listas-Apagar").value);
    montaTolltip('container-tooltip', 'tootip', 'conteudo-tooltip', 'Lista apagada com sucesso!', 'green');
    fecharModal();
}

export {apagarLista}