import * as modalLocalStorage from '../componentes/modal/ModalSalvarLS.js'
import * as modalLocalStorageApagar from '../componentes/modal/ModalApagarLS.js'
import * as modalLocalStorageCarregar from '../componentes/modal/ModalCarregarLS.js'
import * as popUpEffect from './popUp.js'
import * as montaComponent from './montaComponente.js'
import {alterarQntLinhasPorPag} from './paginacao.js'

//Funções utilizadas no html 

window.alterarQntLinhasPorPag = alterarQntLinhasPorPag;

window.popUpEffect = {
    abrirPopUpEfeitoOpacidade: popUpEffect.abrirPopUpEfeitoOpacidade,
    abrirMoreOptions: popUpEffect.abrirMoreOptions,
    fecharPopUpEfeitoOpacidade: popUpEffect.fecharPopUpEfeitoOpacidade,
    fecharMoreOptions: popUpEffect.fecharMoreOptions,
    importarListaJson: popUpEffect.importarListaJson
}

window.montaComponent = {
    montaModalLocalStorage: montaComponent.montaModalLocalStorage, 
    montaTolltip: montaComponent.montaTolltip
}

//Funções dos botões do modal de salvar no localStorage
window.funcaoModalLS = {
    salvarLista: modalLocalStorage.salvarLista, 
    cancelar: modalLocalStorage.cancelar, 
    sobrescrever: modalLocalStorage.sobrescrever, 
    concatenar: modalLocalStorage.concatenar, 
    apagarLista: modalLocalStorage.apagarLista,
    toogleInputNome: modalLocalStorage.toogleInputNome,
    cancelarConcat: modalLocalStorage.cancelarConcat,
    finalizarConcat: modalLocalStorage.finalizarConcat,
    fecharModal: modalLocalStorage.fecharModal,
    modalApagarLista: modalLocalStorageApagar.apagarLista,
    carregarLista: modalLocalStorageCarregar.carregarLista
}
