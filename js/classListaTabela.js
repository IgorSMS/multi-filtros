export class tabela {
    constructor(lista, idTabela, labels = [], paginaAtual = 1, qntPorPag = 10, nomeLista = 'Tabela-Multi-Filtros') {
        this._labelColunas = labels;
        this._dadosListaTabela = lista;
        this._cloneListaTabela = [...lista];
        this._paginaAtual = paginaAtual;
        this._qntPorPag = qntPorPag;
        this._nomeLista = nomeLista;
        this._tabela = document.getElementById(idTabela);
    }

    getLista = () => this._dadosListaTabela;
    setLista = dados => this._dadosListaTabela = dados;

    getNomeLista = () => this._nomeLista;
    setNomeLista = dados => this._nomeLista = dados;

    getListaClone = () => this._cloneListaTabela;
    setListaClone = dados => this._cloneListaTabela = dados;

    getLabels = () => this._labelColunas;
    setLabels = dados => this._labelColunas = dados;
    
    getPaginaAtual = () => this._paginaAtual;
    setPaginaAtual = dados => this._paginaAtual = dados;

    getQntPagina = () => this._qntPorPag;
    setQntPagina = dados => this._qntPorPag = dados;

    getTagTabela = () => this._tabela;
    setTagTabela = dados => this._tabela = dados;
    
    inverterOrdemLista = () => this._cloneListaTabela.reverse();

    setOrdemAlfabetica = (indexColuna) => this._cloneListaTabela.sort((proximoElemento, primeiroElemento) => {
            var nomeProximoElemento = Object.values(proximoElemento)[indexColuna].toUpperCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "");
            var nomePrimeiroElemento = Object.values(primeiroElemento)[indexColuna].toUpperCase().normalize("NFD").replace(/[\u0300-\u036f]/g, "");
            return (nomeProximoElemento < nomePrimeiroElemento) ? -1 : (nomeProximoElemento > nomePrimeiroElemento) ? 1 : 0;
    });

    resetCloneLista = () => this._cloneListaTabela = [...this._dadosListaTabela]
}