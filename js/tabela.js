import {tabela as classTabela} from './classListaTabela.js';
import {paginacao} from './paginacao.js'
import {removerTagsAdicionais, removerTodasTagsFilhas} from './reutilizaveis.js'

var labelColunas = [{ col1: "AlphaCode", col2: "Continente", col3: "Pais", col4: "Capital", col5: "numericCode"}];
var tabela = new classTabela([], 'idTabela', labelColunas);

let clicarLinha = function () {
    console.warn("Sem evento atribuído ao click da linha!!");
};

function popularTabela(linhas, titulos, porFiltros = false, funcaoClicarNaLinha) {

    if (funcaoClicarNaLinha)
        clicarLinha = funcaoClicarNaLinha;

    if (!titulos){  
        linhas = paginacao(filtrarTabela(linhas), tabela.getQntPagina(), porFiltros ? 1 : tabela.getPaginaAtual());
        removerTagsAdicionais(tabela.getTagTabela());
    }
    linhas.forEach(linha => {
        let tagTr = document.createElement("tr");
        Object.entries(linha).forEach((chaveValor, index) => {
        let conteudoTr = document.createElement(titulos ? "th":"td");
        if (titulos){
            criarFiltros(chaveValor[1]);
            conteudoTr.setAttribute("id", chaveValor[0]);
            conteudoTr.classList.add("titulo-coluna", "usr-slct-none");
            conteudoTr.addEventListener("click", function() {
                ordenarTabela(index ,this);
            });
        }else{
            tagTr.setAttribute("class", "linhas-tabela");
        }
        conteudoTr.innerHTML = `<div class="dsp-inline-blk">${chaveValor[1]}</div>`;
        tagTr.appendChild(conteudoTr);
        });
        if (!titulos)
        tagTr.addEventListener('click', clicarLinha.bind(linha));
        tabela.getTagTabela().appendChild(tagTr);
        if (!porFiltros){
            tagTr.classList.add("trns-rotate-y-opct-0");
            setTimeout(() => {
                tagTr.classList.add("trns-rotate-y-opct-1")
            }, 10);
        }else {
            tabela.setPaginaAtual(1);
        }
    });
}

function filtrarTabela(lista){
    let inputs = document.getElementsByClassName('inputs-filters');
    let inputsPreenchidos = [];

    if (inputs.length == 0)
        titulosColunasSemLabel(lista[0]);

    Array.from(inputs).forEach(conteudo => {
        inputsPreenchidos.push({
            preenchido: conteudo.value != "",
            valor: conteudo.value
        });
    });

    return lista.filter(linha => {
        let verificaLinha = true;
        Object.entries(linha).forEach((valorColuna, index) => {
            if (index >= inputsPreenchidos.length ) return 
            if (inputsPreenchidos[index].preenchido && verificaLinha)
                verificaLinha = valorColuna[1].toLowerCase().includes(inputsPreenchidos[index].valor.toLowerCase());
        });
        return verificaLinha
    });
}

function ordenarTabela(indexColuna, colunaClicada){
    if (`&#${colunaClicada.lastElementChild.innerHTML.charCodeAt(0)}` == '&#8638'){
        removerTagsAdicionais(colunaClicada);
        tabela.setListaClone([...tabela.getLista()]);
        popularTabela(tabela.getLista(), false);
        return
    }
    if (colunaClicada.childNodes.length > 1){
        colunaClicada.lastElementChild.innerHTML = '&#8638';
        popularTabela(tabela.inverterOrdemLista(), false);
        return
    }
    tabela.setOrdemAlfabetica(indexColuna);

    popularTabela(tabela.getListaClone(), false);
    inserirIconeTituloColuna(colunaClicada);

}

function inserirIconeTituloColuna(elementoTitulo){
    removeIconesTituloColuna();
    let tagDiv = document.createElement("div");
    tagDiv.innerHTML = '&#8643';
    tagDiv.setAttribute("class", "dsp-inline-blk flt-r fnt-sz-14-px");
    elementoTitulo.appendChild(tagDiv);
}

function removeIconesTituloColuna(){
    let todosTitulos = document.getElementsByClassName("titulo-coluna");
    Array.from(todosTitulos).forEach(element => removerTagsAdicionais(element));
}

function criarFiltros(nomeColuna){

    let labelInputTag = document.createElement('label');
    labelInputTag.innerHTML = `<i>${nomeColuna}</i>`;

    let inputTag = document.createElement('input');
    inputTag.classList.add('mrg-top-10-px', 'bg-transp', 'fnt-sz-1-rem', 'outline-none', 'border-none', 'inputs-filters');
    inputTag.setAttribute('placeholder', 'Digite aqui...');
    inputTag.setAttribute('type', 'text');
    inputTag.setAttribute('id', nomeColuna);
    inputTag.addEventListener('input', () => {
        tabela.resetCloneLista();
        popularTabela(tabela.getLista(), false, true); 
        removeIconesTituloColuna();
    });

    let hrTag = document.createElement('hr');
    hrTag.classList.add('bg-linear-grad-blck-trasp', 'mrg-left-unset', 'w-100-perc', 'border-none', 'hght-1-px');

    let divInput = document.createElement('div');
    divInput.classList.add('dsp-flx', 'flx-dir-col', 'mrg-xy-5-px');
    divInput.appendChild(labelInputTag);
    divInput.appendChild(inputTag);
    divInput.appendChild(hrTag);

    document.getElementById('filtros-tabela').appendChild(divInput);
}

function titulosColunasSemLabel(linha) {
    let titulosColunaSemLabel = {};
    Object.keys(linha).forEach(key => titulosColunaSemLabel[key] = key);
    popularTabela([titulosColunaSemLabel], true);
}

function limparTabela(idTabela, idFiltros){
    popularTabela([], false, false, function() {
        console.warn("Sem evento atribuído ao click da linha!!");
    });
    removerTodasTagsFilhas(document.getElementById(idTabela));
    removerTodasTagsFilhas(document.getElementById(idFiltros));
}

function novaTabela(listaNova, idTabela, labels = [], paginaAtual = 1, qntPorPag = 10, nomeLista = 'Tabela-Multi-Filtros') {
    limparTabela('idTabela', 'filtros-tabela');
    tabela = new classTabela(listaNova, idTabela, labels, paginaAtual, qntPorPag, nomeLista);
    popularTabela(tabela.getListaClone(), false);
    document.getElementById('qntPPag').value = '';
}


document.getElementById('button-export').addEventListener('click', () => {
    let matriz = [];
    
    tabela.getListaClone().forEach(conteudo => {
        let linhasExcel = [];
        Object.values(conteudo).forEach(valor => {
            linhasExcel.push(valor)
        });
        matriz.push(linhasExcel);
    });
    
    var csv = '';
    Object.keys(tabela.getListaClone()[0]).forEach(key => {
        csv += key + ';'
    })
    csv += '\n';
    matriz.forEach(row => {
            csv += row.join(';');
            csv += "\n";
    });
    var hiddenElement = document.createElement('a');
    hiddenElement.href = 'data:text/csv;charset=utf-8,' + "\uFEFF" + encodeURI(csv);
    hiddenElement.target = '_blank';
    hiddenElement.download =  tabela.getNomeLista() + '.csv';
    hiddenElement.click();
});


export {popularTabela, tabela, limparTabela, novaTabela}