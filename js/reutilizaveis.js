function removerTagsAdicionais(tagPai){
    while (tagPai.childNodes.length > 1)
        tagPai.removeChild(tagPai.lastElementChild);
}

function removerTodasTagsFilhas(tagPai){
    while (tagPai.childNodes.length >= 1)
        tagPai.removeChild(tagPai.lastElementChild);
}

function getInfoLocalstorage(data) {
    return JSON.parse(localStorage.getItem(data))
}

function setInfoLocalstorage(data, value) {
    localStorage.setItem(data, JSON.stringify(value));
}

export {
    removerTagsAdicionais,
    removerTodasTagsFilhas,
    getInfoLocalstorage,
    setInfoLocalstorage
}