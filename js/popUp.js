import {novaTabela} from './tabela.js'

function abrirPopUpEfeitoOpacidade(idElement){
    document.getElementById(idElement).classList.remove('dsp-none');
    document.getElementById(idElement).classList.add('z-index-1');
    setTimeout(() => {
        document.getElementById(idElement).classList.add('opacity-1');
    }, 10);
}

function fecharPopUpEfeitoOpacidade(idElement){
    document.getElementById(idElement).classList.remove('opacity-1');
    setTimeout(() => {
        document.getElementById(idElement).classList.remove('z-index-1');
        document.getElementById(idElement).classList.add('dsp-none');
    }, 250);
}

function abrirMoreOptions() {
    document.getElementById('more-options').classList.remove('dsp-none');
    setTimeout(() => {
        document.getElementById('more-options').classList.add('mx-hght-500-px');
        document.getElementById('more-options').classList.remove('mx-hght-0-px');
        document.getElementById('content').addEventListener('click', fecharMoreOptions)
    }, 10);
}

function fecharMoreOptions() {
    document.getElementById('more-options').classList.remove('mx-hght-500-px');
    document.getElementById('more-options').classList.add('mx-hght-0-px');
    setTimeout(() => {
        document.getElementById('more-options').classList.add('dsp-none');
    }, 200);
    document.getElementById('content').removeEventListener('click', fecharMoreOptions)
}

function importarListaJson() {
    fecharPopUpEfeitoOpacidade('pop-up-import');
    novaTabela(JSON.parse(document.getElementById('lista-json').value), 'idTabela', [], 1, 10);
    document.getElementById('qntPPag').value = '';
}

document.getElementById('button-import').addEventListener('click', () => {
    abrirPopUpEfeitoOpacidade('pop-up-import')
});

export {
    abrirPopUpEfeitoOpacidade,
    abrirMoreOptions,
    fecharPopUpEfeitoOpacidade,
    fecharMoreOptions,
    importarListaJson
}