import {popularTabela, tabela} from './tabela.js'
import {removerTodasTagsFilhas} from './reutilizaveis.js'

function paginacao(lista, totalItensPorPagina = 10, posicaoPagina = 1){
    document.getElementById("total-registros").innerText = `Total de registros: ${lista.length}`
    let totalPaginas = 0;
    if (lista.length % totalItensPorPagina) totalPaginas = ~~(lista.length / totalItensPorPagina) + 1
    else totalPaginas = ~~(lista.length / totalItensPorPagina)

    controlePaginacao(totalPaginas, posicaoPagina)

    return lista.filter((linha, index) => {
        return index >= totalItensPorPagina * posicaoPagina - totalItensPorPagina && index < totalItensPorPagina * posicaoPagina
    });
}

function controlePaginacao(qntDePag, posicaoPagina, qntMostradores = 7){
    let grupoNumerosPaginas = document.getElementById("qnt-paginas");
    removerTodasTagsFilhas(grupoNumerosPaginas)
    let numeroPaginacao;
    for (let i = 0; qntDePag > qntMostradores ? i < qntMostradores : i < qntDePag ; i++) {
        if (posicaoPagina <= qntDePag - 3 || qntDePag <= qntMostradores){
            if (i > 0 && i < qntMostradores - 1) 
                numeroPaginacao = qntDePag > qntMostradores && posicaoPagina > 4 ? i + 1 + posicaoPagina -4 : i + 1;
            else 
                numeroPaginacao = i == 0 ? 1 : qntDePag;
        }else if (i == 0){
            numeroPaginacao = 1
        }
        else{
            numeroPaginacao = qntDePag - qntMostradores + i + 1;
        }

        let tagNumeroPaginacao = document.createElement("a");
        if (numeroPaginacao == posicaoPagina) {
            tagNumeroPaginacao.classList.add("active")
        }else{
            tagNumeroPaginacao.addEventListener('click', function() {
                tabela.setPaginaAtual(parseInt(this.innerText));
                popularTabela(tabela.getListaClone(), false);
            });
        }
        tagNumeroPaginacao.innerText = numeroPaginacao;
        grupoNumerosPaginas.appendChild(tagNumeroPaginacao);
    }
}

function alterarQntLinhasPorPag(valor) {
    tabela.setPaginaAtual(1);
    tabela.setQntPagina(valor ? parseInt(valor) : 10);
    popularTabela(tabela.getListaClone(), false);
}

export {paginacao, controlePaginacao, alterarQntLinhasPorPag}