import {abrirPopUpEfeitoOpacidade, fecharPopUpEfeitoOpacidade, fecharMoreOptions} from './popUp.js'
import {obterComponents} from './services.js'

async function montaModalLocalStorage(idModal, tipoModal) {
    document.getElementById(idModal).innerHTML = await obterComponents(`componentes/modal/Modal${tipoModal}LS.html`);
    let openModal = true;
    if (tipoModal != "Salvar"){
        if (localStorage.length) {
            let selectListas = document.getElementById(`select-listas-${tipoModal}`);
            Object.keys(localStorage).forEach(nomeLista => {
                var opt = document.createElement('option');
                opt.value = nomeLista;
                opt.innerHTML = nomeLista;
                selectListas.appendChild(opt);
            });
        }else{
            openModal = false;
            montaTolltip('container-tooltip', 'tootip', 'conteudo-tooltip', 'Nenhuma lista salva!', 'red');    
        }
    }
    if (openModal){
        fecharMoreOptions();
        abrirPopUpEfeitoOpacidade(idModal);
    }
}

var timeOutTooltip;
async function montaTolltip(idContainerTooltip, idTooltip, idTexto, texto, cor) {
    document.getElementById(idContainerTooltip).innerHTML = await obterComponents('componentes/tooltip/tooltip.html');
    document.getElementById(idTexto).innerText = texto;
    document.getElementById(idTooltip).classList.add('bg-' + cor);
    abrirPopUpEfeitoOpacidade(idTooltip);
    clearTimeout(timeOutTooltip);
    timeOutTooltip = setTimeout(() => {
        fecharPopUpEfeitoOpacidade(idTooltip);
    }, 3000);
}

export {montaModalLocalStorage, montaTolltip}