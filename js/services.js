
function obterListaPaises() {
    return fetch("https://restcountries.eu/rest/v2/all").then(res => res.json());
}

function obterInformacaoPaisPorId(idPais) {
    return fetch(`https://restcountries.eu/rest/v2/alpha/${idPais}`).then(res => res.json());
}

function obterComponents(path){
    return fetch(path, {cache: "no-cache"}).then(resp => resp.text())
}

export {
    obterListaPaises,
    obterInformacaoPaisPorId,
    obterComponents
}