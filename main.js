
import {tabela, popularTabela} from './js/tabela.js'
import {obterListaPaises, obterInformacaoPaisPorId} from './js/services.js'

async function obterInformacaoPais() {
    let dadosPais = await obterInformacaoPaisPorId(Object.values(this)[0]).then(res => res).catch(err => {
        console.log(err);
        return
    });
        
    document.getElementById('label-bandeira').innerHTML = `Bandeira: ${dadosPais.translations.br}`
    let _img = document.getElementById('img-bandeira');
    _img.src = dadosPais.flag;
    _img.onload = function() {
        let containerModal = document.getElementById('modal-container');
        containerModal.removeAttribute("class");
        containerModal.setAttribute("class", "four");
        document.body.setAttribute("class","modal-active");
    }
}

obterListaPaises().then(data => {
    return data.map(pais => {
        return {
            coluna1: pais.alpha3Code || "",
            coluna2: pais.region,
            coluna3: pais.translations.br,
            coluna4: pais.capital,
            coluna5: pais.numericCode || ""
        }
    });
    }).catch(err => {
        console.log(err);
    }).then(listaPaises => {
        tabela.setLista(listaPaises);
        tabela.setListaClone([...listaPaises]);
        popularTabela(tabela.getLabels(), true);
        popularTabela(tabela.getLista(), false ,false, obterInformacaoPais);
    });

document.getElementById('modal-container').addEventListener('click',function(){
    this.classList.add('out');
    document.body.classList.remove('modal-active');
});

//Cartão de visita...
console.table({
    Nome: "Igor Siqueira Marques",
    eMail: "igorsiqueira1@hotmail.com",
    urlGitLab: "https://gitlab.com/IgorSMS/multi-filtros",
    LinkedIn: "https://www.linkedin.com/in/igor-silva-b05734164/"
})
console.log('https://gitlab.com/IgorSMS/multi-filtros');
console.log('https://www.linkedin.com/in/igor-silva-b05734164/');