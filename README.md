# Multi-Filtros

Para o projeto rodar local é necessário subir a pasta raiz em uma porta;
**Obs**: Utilizo o http-server
- instalação: npm i -g http-server
- comando para subir o server: http-server -c-1

Este projeto foi construído utilizando somente as tecnologias: html, css, javascript (vanilla);

Objetivo pessoal com o projeto: Estudos.

Características do projeto: 
1. Monta uma tabela a partir de dados obtidos de uma api;
2. Funções de filtros;
3. Ordenar ao clicar nos títulos das colunas (Ordem alfabética);
4. Destacar linha ao passar o mouse em cima da mesma;
5. Funções de paginação;
6. Exibe informação adicional ao clicar na linha;
7. Efeitos de trasição com o conteúdo;
8. Opções de salvar, concatenar, excluir ou sobrescrever a lista do localstorage;

**Não será utilizado nenhuma lib externa nesse projeto**

Até o momento o projeto não é responsivo

link do projeto: https://multi-filtros.netlify.app/